// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

InternetButton button = InternetButton();
int questionNo=1;
void setup() {
  button.begin();

  // Exposed functions-test


  Particle.function("score", displayScore);
 
  Particle.function("answer", showCorrectOrIncorrect);
  Particle.function("shapeLight", showShapeLight);
  Particle.function("allLightsOff", showAllLightsOff);
  

  // Show a visual indication to the player that the Particle
  // is loaded & ready to accept inputs
  for (int i = 0; i < 3; i++) {
    button.allLedsOn(20,20,20);
    delay(250);
    button.allLedsOff();
    delay(250);
  }
}

int showCorrectOrIncorrect(String cmd) {
  if (cmd == "green") {
    button.allLedsOn(0,255,0);
    delay(2000);
    button.allLedsOff();
  }
  else if (cmd == "red") {
    button.allLedsOn(255,0,0);
    delay(2000);
    button.allLedsOff();
  }
  else {
    // you received an invalid color, so
    // return error code = -1
    return -1;
  }
  // function succesfully finished
  return 1;
}

int DELAY = 100;

void loop() {

  // EVENT 1:
  

  if (button.buttonOn(4)) {
    
    Particle.publish("firstQuestion","D", 60, PRIVATE);
    delay(DELAY);
  }

  if (button.buttonOn(2)) {
   
    Particle.publish("firstQuestion","B", 60, PRIVATE);
    delay(DELAY);
  }
//   if (button.buttonOn(1)) {
   
//     Particle.publish("firstQuestion","false", 60, PRIVATE);
//     delay(DELAY);
//   }

  // EVENT 2:

//   if (button.buttonOn(4)) {
    
//     Particle.publish("secondQuestion","false", 60, PRIVATE);
//     delay(DELAY);
//   }
   if (button.buttonOn(3)) {
   
    Particle.publish("secondQuestion","C", 60, PRIVATE);
    delay(DELAY);
  }
  if (button.buttonOn(2)) {
   
    Particle.publish("secondQuestion","B", 60, PRIVATE);
    delay(DELAY);
  }
//   if (button.buttonOn(1)) {
   
//     Particle.publish("secondQuestion","false", 60, PRIVATE);
//     delay(DELAY);
//   }
  
  //Event 3:
    if (button.buttonOn(1)) {
   
    Particle.publish("displayScore","score", 60, PRIVATE);
    delay(DELAY);
  }
}

int displayScore(String cmd) {
  // reset the displayed score
  button.allLedsOff();

  // turn on the specified number of lights
  int score = cmd.toInt();

  if (score < 0 || score > 11) {
    // error: becaues there are only 11 lights
    // return -1 means an error occurred
    return -1;
  }

  for (int i = 1; i <= score; i++) {
      button.ledOn(i, 255, 255, 0);
  }
  delay(2000);
  button.allLedsOff();
  // return = 1 means the function finished running successfully
  return 1;
}

int showShapeLight(String cmd) {
    
    if(cmd=="rectangle"){
        button.allLedsOff();
        button.ledOn(2, 255, 0, 0);
        button.ledOn(10, 255, 0, 0);
        button.ledOn(4, 255, 0, 0);
        button.ledOn(8, 255, 0, 0);
        
    }else if(cmd=="triangle"){
          button.allLedsOff();
          button.ledOn(2, 0, 0, 255);
        button.ledOn(10, 0, 0, 255);
        button.ledOn(6, 0, 0, 255);
       
        
    }
}

int showAllLightsOff(String cmd) {
    button.allLedsOff();
}



